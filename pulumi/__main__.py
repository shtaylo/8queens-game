import pulumi
import pulumi_aws as aws

size = 't2.micro'

ami = aws.get_ami(most_recent="true",
                  owners=["099720109477"],
                  filters=[{"name":"name","values":["ubuntu/images/hvm-ssd/ubuntu-jammy-22.04-amd64-server-20230516"]}])

vpc = aws.ec2.Vpc('server-vpc', cidr_block='10.0.0.0/16')

gateway = aws.ec2.InternetGateway('server-gateway', vpc_id=vpc.id)

subnet = aws.ec2.Subnet('server-subnet', vpc_id=vpc.id, cidr_block='10.0.1.0/24', map_public_ip_on_launch=True)

routes = aws.ec2.RouteTable('server-routes', vpc_id=vpc.id, routes=[{ 'cidrBlock': '0.0.0.0/0', 'gatewayId': gateway.id }])

route_table_association = aws.ec2.RouteTableAssociation('route-table-association', subnet_id=subnet.id, route_table_id=routes.id)

group = aws.ec2.SecurityGroup('server-secgrp', vpc_id=vpc.id, ingress=[
    { 'protocol': 'tcp', 'from_port': 22, 'to_port': 22, 'cidr_blocks': ['0.0.0.0/0'] },
    { 'protocol': 'tcp', 'from_port': 80, 'to_port': 80, 'cidr_blocks': ['0.0.0.0/0'] },
])

with open('./ec2script.txt', 'r') as runtime_script:
    user_data = runtime_script.read()

server = aws.ec2.Instance('server-instance',
    instance_type=size,
    vpc_security_group_ids=[group.id],
    ami=ami.id,
    user_data=user_data,
    subnet_id=subnet.id)

pulumi.export('publicIp', server.public_ip)