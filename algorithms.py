# Function checks if the given queen is valid.
# --------------------------------------------
def isQueenValid(i, j, chess_board):
    for x in range(0, 8):
        # Horizontal
        if (chess_board[i][x] == 1) and x != j:
            return False
        
        # Vertical
        if (chess_board[x][j] == 1) and x != i:
            return False
        
        # Lower/Left
        if (i + x) < 8 and (j - x) > -1 and x != 0:
            if chess_board[i + x][j - x] == 1:
                return False

        # Upper/Left
        if (i - x) > -1 and (j - x) > -1 and x != 0:
            if chess_board[i -x][j - x] == 1:
                return False

        # Lower/Right
        if (i + x) < 8 and (j + x) < 8 and x != 0:
            if chess_board[i + x][j + x] == 1:
                return False
        
        # Upper/Right
        if (i - x) > -1 and (j + x) < 8 and x != 0:
            if chess_board[i-x][j + x] == 1:
                return False
    return True
# --------------------------------------------


# Function finds the first solution
# --------------------------------------------
def firstSolution(chess_board):
    queens_counter = 0
    i = 0 # Loop index

    while i < 8: # First loop
        found_in_row = False
        j = 0 # Second loop index
        while j < 8: # Second loop
            chess_board[i][j] = 1 # Sets a new queen
            
            if isQueenValid(i, j, chess_board): # If queen is valid
                queens_counter += 1 # Update counter
                found_in_row = True
                break # Continue to the next row
            else:
                chess_board[i][j] = 0  # Remove the new queen
                
                # If the whole row can't contain a queen, go back to the previous row
                if j == 7 and i - 1 > -1 and found_in_row == False:
                    i -= 1
                    x = -1 # Loop index
                    while x < 8:
                        x += 1

                        # Search for the queen in the (prev) row and remove it,
                        # then continue to the next position to try and set a new queen
                        # Reason: the queen is not valid.
                        if chess_board[i][x] == 1:
                            chess_board[i][x] = 0 # Remove the queen
                            queens_counter -= 1 # Update counter

                            # If the queen is found in the last column,
                            # then remove it and go back a row again
                            if x == 7:
                                x = -1
                                i -= 1
                            else: # Otherwise, update index and continue to the next column.
                                j = x
                                break
            j += 1 # Index update
        i += 1 # Index update
    return chess_board
# --------------------------------------------


# Function finds the next solution
# --------------------------------------------
def nextSolution(chess_board, last_queen):
    queens_counter = 7
    # Removing the last queen
    chess_board[last_queen[0]][last_queen[1]] = 0

    # Settings the indexes according to the last queen's position.
    if last_queen[1] < 7:
        i = last_queen[0]
        j = last_queen[1] + 1
    else:
        i = last_queen[0] - 1
        j = 0

 
    while i < 8: # First loop
        found_in_row = False
        while j < 8: # Second loop
            chess_board[i][j] = 1 # Sets a new queen
            
            if isQueenValid(i, j, chess_board): # If queen is valid
                queens_counter += 1 # Update counter
                found_in_row = True
                j = 0
                break # Continue to the next row
            else:
                chess_board[i][j] = 0  # Remove the new queen
                
                # If the whole row can't contain a queen, go back to the previous row
                if j == 7 and i - 1 > -1 and found_in_row == False:
                    i -= 1
                    x = -1 # Loop index
                    while x < 8:
                        x += 1

                        # Search for the queen in the (prev) row and remove it,
                        # then continue to the next position to try and set a new queen
                        # Reason: the queen is not valid.
                        if chess_board[i][x] == 1:
                            chess_board[i][x] = 0 # Remove the queen
                            queens_counter -= 1 # Update counter

                            # If the queen is found in the last column,
                            # then remove it and go back a row again
                            if x == 7:
                                x = -1
                                i -= 1
                            else: # Otherwise, update index and continue to the next column.
                                j = x
                                break
            j += 1 # Index update
        i += 1 # Index update
    return chess_board
# --------------------------------------------

# Function finds the position of the last queen.
# --------------------------------------------
def findLastQueen(chess_board, numberOfQueens):
    queenCounter = 0

    for i in range(len(chess_board)):
        for j in range(len(chess_board[i])):
            if chess_board[i][j] == 1:
                queenCounter += 1

                if queenCounter == numberOfQueens:
                    return [i, j]
# --------------------------------------------


# Main code
# --------------------------------------------
def main():
    chess_board = [[0]*8 for i in range(8)]
    first_solution = firstSolution(chess_board)
    lastQueen = findLastQueen(first_solution, 8)
    next_solution = nextSolution(first_solution, lastQueen)

    # Print board (for tests).
    for i in range(len(first_solution)):
            print("")
            for j in range(len(first_solution[i])):
                print(first_solution[i][j], end=" ")
    print("")

    # Print board (for tests).
    for i in range(len(next_solution)):
            print("")
            for j in range(len(next_solution[i])):
                print(next_solution[i][j], end=" ")
    print("")

# --------------------------------------------

if __name__ == '__main__':
    main()