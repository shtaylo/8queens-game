from flask import Flask

def create_app():
    app = Flask(__name__)
    app.config['SECRET_KEY'] = 'SECRET_KEY'

    from .views import views
    from .game import game

    app.register_blueprint(views, url_prefix='/')
    app.register_blueprint(game, url_prefix='/')

    return app