from flask import Blueprint, render_template, session, redirect, url_for

views = Blueprint('views', __name__)

@views.route('/', methods=['GET', 'POST'])
def home():
    chess_board = [[""]*8 for i in range(8)]
    return render_template("home.html", chess_board=chess_board)