from flask import Blueprint, render_template, session, redirect, url_for
from algorithms import isQueenValid, firstSolution, nextSolution, findLastQueen

game = Blueprint('actions', __name__)
global chess_board_binary
chess_board_binary = [[0]*8 for i in range(8)]

@game.route('/play', methods=['POST'])
def play():
    queen = '♕'
    solution = firstSolution(chess_board_binary)
    chess_board = [[""]*8 for i in range(8)]
    
    for i in range(len(solution)):
        for j in range(len(solution[i])):
            if solution[i][j] == 1:
                chess_board[i][j] = queen

    return render_template("home.html", chess_board=chess_board, start=True)

@game.route('/again', methods=['POST'])
def again():
    queen = '♕'
    lastQueen = findLastQueen(chess_board_binary, 8)
    solution = nextSolution(chess_board_binary, lastQueen)
    chess_board = [[""]*8 for i in range(8)]
    
    for i in range(len(solution)):
        for j in range(len(solution[i])):
            if solution[i][j] == 1:
                chess_board[i][j] = queen

    return render_template("home.html", chess_board=chess_board, start=True)

@game.route('/reset', methods=['POST'])
def reset():
    global chess_board_binary, chess_board
    chess_board_binary = [[0]*8 for i in range(8)]
    chess_board = [[""]*8 for i in range(8)]

    return render_template("home.html", chess_board=chess_board, start=False)